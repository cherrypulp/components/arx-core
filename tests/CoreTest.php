<?php

namespace Arx\Core\Tests;

use Arx\Core\Facades\Core;
use Arx\Core\ServiceProvider;
use Orchestra\Testbench\TestCase;

class CoreTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'core' => Core::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
