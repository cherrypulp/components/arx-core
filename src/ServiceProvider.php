<?php

namespace Arx\Core;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/core.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('core.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'core'
        );

        $this->app->bind('core', function () {
            return new Core();
        });

        $this->app->bind('hook', function () {
            return new Hook();
        });
    }
}
