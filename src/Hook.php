<?php namespace Arx\Core;

use Blok\Utils\Arr;

/**
 * Class Hook
 *
 * Little class to add easily a hook system to any kind of project
 *
 * @example
 *
 * Hook::put('var1.var2', ['data']);
 * Hook::
 *
 * #output
 *
 * @package Arx\classes
 */
class Hook extends \ArrayObject
{
    public static $pref = "";

    public static $logs = array();

    public static $callback = array();

    public static $debug = true;

    public function __get($name)
    {
        return $GLOBALS[ARX_HOOK][$name];
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        return self::put($name, $value);
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     * @throws \Exception
     */
    public function __isset($name)
    {
        return isset($GLOBALS[ARX_HOOK][$name]);
    }

    /**
     * Check if Hook has key
     * @param $key
     * @return bool
     */
    public static function has($key)
    {
        return self::get($key) ? true : false;
    }

    public static function register($name, $callback = null){
        $GLOBALS[ARX_HOOK][$name] = array();
        self::$callback[$name] = array($callback);
    }

    public static function log($action, $name, $params = array()){

        if(!isset($logs[$action])){
            self::$logs[$action] = array();
        }

        self::$logs[$action][$name][] = array(
            'params' => $params,
            'debug' => debug_backtrace(),
        );
    }

    public static function logs(){
        return self::$logs;
    }

    /**
     * Force to set hook value
     *
     * @param $name
     * @param array $value
     * @return mixed
     */
    public static function set($name, $value = array()){
        return $GLOBALS[ARX_HOOK][$name] = $value;
    }

    /**
     * Put data
     *
     * @param $name
     * @param array $value
     * @param null $merge
     * @return mixed
     * @throws \Exception
     */
    public static function put($name, $value = array(), $merge = null){

        if ($pos = strpos($name, '.')) {
            $dots = substr($name, $pos + 1);
            $name = substr($name, 0, $pos);
        }

        if (!isset($GLOBALS[ARX_HOOK][$name])) {
            $GLOBALS[ARX_HOOK][$name] = array();
            self::log('add', $name, func_get_args());
        }

        if(!is_string($value) && $merge === null && !is_bool($value) && !Arr::is_sequential($value)){
            $merge = true;
        }

        if(isset($dots)){
            Arr::set($GLOBALS[ARX_HOOK][$name], $dots, $value);
        } elseif ($merge) {
            if (is_string($value)) {
                $value = array($value);
            }
            $GLOBALS[ARX_HOOK][$name] =  Arr::merge($GLOBALS[ARX_HOOK][$name], $value);
        } elseif (is_array($value)) {
            foreach ($value as $v) {
                if(!in_array($v, $GLOBALS[ARX_HOOK][$name])){
                    $GLOBALS[ARX_HOOK][$name][] = $v;
                }
            }
        } else {
            if(!in_array($value, $GLOBALS[ARX_HOOK][$name])){
                $GLOBALS[ARX_HOOK][$name][] = $value;
            }
        }

        return $GLOBALS[ARX_HOOK][$name];
    }

    /**
     * Load PHP CLASSES
     * @author Daniel Sum
     * @version 0.1
     * @package arx
     * @comments :
     */
    public static function getAll()
    {
        return $GLOBALS[ARX_HOOK];
    }

    /**
     * Output hook
     *
     * @return bool
     */
    public static function output()
    {
        $aArgs = func_get_args();

        $c = $aArgs[0];

        if(isset($GLOBALS[self::$pref.$c])){
            switch (true) {
                case ($c == 'js'):

                    $output = Asset::dump($GLOBALS[self::$pref.$c]);

                    break;
                case ($c == 'css'):
                    $output = Asset::dump($GLOBALS[self::$pref.$c]);
                    break;
                default:
                    $output = $GLOBALS[self::$pref.$c];
                    break;
            }
        } else {
            $output = false;
        }



        return $output;
    }

    /**
     * Get registered hook
     *
     * @param $name
     * @param null $default
     * @return array
     */
    public static function get($name, $default = null){

        $dots = null;

        if ($pos = strpos($name, '.')) {
            $dots = substr($name, $pos + 1);
            $name = substr($name, 0, $pos);
        }

        if(isset($GLOBALS[ARX_HOOK][$name])){

            if ($dots) {
                return Arr::get($GLOBALS[ARX_HOOK][$name], $dots, $default);
            }

            return $GLOBALS[ARX_HOOK][$name];
        }

        if ($default) {
            return $default;
        }

        return [];
    }

    /**
     * Little helper to output hook
     *
     * @param $name
     * @param $default
     */
    public static function ddget($name, $default){
        dd($name, $default);
    }

    /**
     * Output json type
     *
     * @param $name
     * @param $default
     * @return string
     */
    public static function getJson($name, $default = array()){
        return json_encode(self::get($name, $default));
    }

    public static function eput($c){
        echo self::output($c);
    }

    /**
     * Start method put your output in memory cache until you end
     *
     * @param $name
     * @param null $key
     *
     */
    public static  function start($name, $key = null){
        if ($key) {
            $GLOBALS[ARX_HOOK][$name][$key] = '';
        } else {
            $GLOBALS[ARX_HOOK][$name] = '';
        }
        ob_start();
    }

    /**
     * End your cached data and save it in a globals
     * @param $name
     * @param null $key
     */
    public static function end($name, $key = null){

        if ($key) {
            $GLOBALS[ARX_HOOK][$name][$key] = ob_get_contents();
        } else {
            $GLOBALS[ARX_HOOK][$name] = ob_get_contents();
        }

        ob_end_clean();
    }
}

/**
 * Register a global arx_hook element (needed to have a global element)
 */

if(!defined('ARX_HOOK')) define('ARX_HOOK', 'ARX_HOOK');

if (!isset($GLOBALS[ARX_HOOK])) {
    $GLOBALS[ARX_HOOK] = new Hook();
}
