<?php

namespace Arx\Core;

class Core
{
    /**
     * Return the environnement level of the current app
     *
     * @comments
     *
     * Convention is :
     * - 0 = local environment : local db
     * - 1 = local environment : local db
     * - 2 = develop environment : server dev, db dev
     * - 3 = staging environment : server demo, db dev
     * - 4 = acceptance environment :
     *
     * @return Integer
     */
    public static function getEnvironmentLevel()
    {
        /**
         * Allow at the lowest level to override the default settings
         *
         * @comments please be aware to return an integer at the end whatever your logic !
         */
        if (function_exists('arx_override_get_environment_level')) {
            return arx_override_get_environment_level();
        }

        switch (app()->environment()) {
            case "local":
                return 0;
            case "loc":
                return 1;
            case "develop":
                return 2;
            case "staging":
                return 3;
            case "acceptance":
                return 4;
            case "production":
                return 5;
            default:
                return -1;
        }
    }
}
