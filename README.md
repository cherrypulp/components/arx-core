# Laravel Extended Core Package

[![Packagist](https://img.shields.io/packagist/v/arx/core.svg)](https://packagist.org/packages/arx/core)
[![Packagist](https://poser.pugx.org/arx/core/d/total.svg)](https://packagist.org/packages/arx/core)
[![Packagist](https://img.shields.io/packagist/l/arx/core.svg)](https://packagist.org/packages/arx/core)

Laravel Extended Core packages to kickstart your project

## Installation

Install via composer
```bash
composer require arx/core=^8.0
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Arx\Core\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Arx\Core\Facades\Core::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Arx\Core\ServiceProvider" --tag="config"
```

## Usage

Arx Core is a bunch of selected core package to kickstart your Laravel project.

It includes :

  - "laravel/jetstream": "^2.0"
  - "laravel/ui": "^3.2"
  - "blok/utils": "^1.0"
  - "cherrypulp/laravel-action-filter": "^1.0"
  - "tymon/jwt-auth": "^1.0.1"
  - "spatie/laravel-medialibrary": "^8.7.2"
  - "spatie/laravel-permission": "^3.17"

For more infos, just check the dock of each core packages.

If you want to add a new core packages, just make a pull request !

## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- [Cherry Pulp](https://gitlab.com/cherrypulp/components/arx-core)
- [All contributors](https://gitlab.com/cherrypulp/components/arx-core/graphs/contributors)
